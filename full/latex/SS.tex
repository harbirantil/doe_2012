%!TEX root = proposal.tex

%------------------%
\section{Stokes FBP and Applications} \label{sec:stokes_fbp}
%------------------%
%The level of complication keeps increasing as move from Subsection~\ref{subsec:ss}-\ref{subsec:extrusion}.
I will describe various projects in Subsections~\ref{subsec:ss}-\ref{subsec:extrusion}. I will start with 
simpler one and continue with more challenging ones. However the tools and results from simpler
ones are essential to tackle the more complicated models.
For example, studying the Laplace equation with surface tension effects 
in Subsection \ref{subsec:ss} is essential to understand the Newtonian (constant viscosity) Stokes 
FBP with surface tension effects in Subsection \ref{subsec:stokes}, which is further
crucial to study the extrusion problem in Subsection \ref{subsec:extrusion} where the 
underlying model is non-Newtonian Stokes FBP.
%I plan to work in a systematic manner from Subsection~\ref{subsec:ss}-\ref{subsec:extrusion}.
%This section is organized as follows: 
%In Subsection \ref{subsec:ss}

Next in Subsection \ref{subsec:ss}, I will present optimal control of a model FBP, including my previous
work. The model FBP can be written as an optimal control problem with 
variational inequalities (VIs). I will describe AFEM for VIs and the propose
the optimization techniques using semi-smooth Newton methods.  
%In Subsection~\ref{subsec:stokes} I present
%an extension to Stokes equations. Here we assume that the 
%fluid viscosity is constant. Finally, in Subsection~\ref{subsec:extrusion}
%I present a real life application where I am interested in polymer
%extrusion problem. The underlying FBP is the Stokes equation with variable
%viscosity. 


%------------------%
\subsection{Control of a Model FBP} \label{subsec:ss}
%------------------%
In \cite{HAntil_RHNochetto_PSodre_2012a} I examined
optimal control to a prototype FBP, 
due to Saavedra and Scott \cite{PSaavedra_RScott_1991}, which
exhibits some of the important features of the FBP discussed in this proposal. 
A viscous fluid is confined in the container 
$\Omegag =\{(x_1,x_2): 0 < x_1 < 1, 0< x_2 < 1+ \gamma(x_1)\}$ with
interface $\Gamma_{\textrm{free}} =\{(x_1,x_2): 0 < x_1 < 1, x_2=1+\gamma(x_1)\}$
separating the fluid from air and governed by surface 
tension. If
$y$ mimics the behavior of velocity, and $u_1$ and $u_2$ represent
control variables, then the (simplified) equations are \looseness=-1
%
\begin{equation}\label{eq:fbp_strong}
-\lapl y = u_1 \quad\text{in } \Omegag,
\qquad
-s \frac{\gamma''(x_1)}{\sqrt{1+|{\gamma}'(x_1)|^2}} = - \nd
y(x_1,1+\gamma(x_1))  + u_2(x_1)
\quad\text{in } (0,1),
\end{equation}
%
with boundary conditions $y = g$ on $\bdy\Omegag$, and 
$\gamma(0) = \gamma(1) = 0$, and $s$ is the surface tension constant and the state and gradient 
constraint to avoid topological changes:
%
\begin{align} \label{eq:SS_state_const}
    \|\gamma\|_{W^1_\infty(0,1)} \le 1.
\end{align}
%
If $g$ is suitably small,
\begin{align} \label{eq:small_data_g}
  \|g\|_{W^1_p(\bdy\Omega_\gamma)} < \epsilon ,
\end{align}
then it was shown in \cite{HAntil_RHNochetto_PSodre_2012a}
that \eqref{eq:fbp_strong} has a unique solution $\gamma
\in \mathring{W}^1_\infty(0,1)$ and $y \in W^1_p(\Omegag)$ with
$p>2$. Note that \eqref{eq:fbp_strong} is formulated in Sobolev
spaces, instead of H\"older spaces \cite{VASolonnikov_1982a}. This is
convenient for developing both a control theory and associated 
numerical methods using FEM.
Moreover, perturbing $u_1$ and/or $u_2$ the free-surface $\gamma$ also
changes and so does the domain $\Omegag$.

Suppose we are given functions $\gamma_d:(0,1)\to \mathbb R$ and $y_d:\Omega^*
\to \mathbb R$, where $\gamma_d$
represents the desired shape of the free surface and $y_d$ the desired
flow distribution on $\Omega^* = (0,1)\times(0,2)$. Furthermore, we
are also given two sets $\mathcal{U}_1\subset L^p(\Omegag)$ and
$\mathcal{U}_2\subset L^p(0,1)$ representing the admissible sets of
our control functions. Then, the mathematically rigorous problem of
finding the optimal controls $u_1 \in \mathcal{U}_1$ and $u_2 \in
\mathcal{U}_2$ which drives the system towards the desired shape
$\gamma_d$ and the desired state $y_d$ is equivalent to minimizing the
functional 
%
\begin{equation}\label{eq:fbp_cost}
\mathcal{J}(y,\gamma, u_1, u_2) = \frac{1}{2} \|y - y_d\|_{L^2(\Omegag)}^2 
+ \frac{1}{2} \|\gamma - \gamma_d\|_{L^2(0,1)}^2 
+ \frac{\lambda_1}{2} \|u_1\|_{L^2(\Omega^*)}^2 
+ \frac{\lambda_2}{2} \|u_2\|_{L^2(0,1)}^2 ,
\end{equation}
%
subject to the state system \eqref{eq:fbp_strong} and
$y \in  W^1_p(\Omegag)$, $\gamma \in \mathring{W}^1_\infty(0,1)$, 
$u_1 \in \mathcal{U}_1$ and $u_2 \in \mathcal{U}_2$. 
%
Next I will state my recent results from \cite{HAntil_RHNochetto_PSodre_2012a} and 
will describe necessary future work for this problem.
\begin{itemize}
\item In \cite{HAntil_RHNochetto_PSodre_2012a} I developed a complete second order analysis for 
          \eqref{eq:fbp_cost}, 
          based on Tr\"oltzsch \cite{FTroltzsch_2010a}, which is absent in the existing literature on optimal
          control for FBP.

\item The analysis hinges on a convex constraint on the control such that the state and gradient
          constraints \eqref{eq:SS_state_const} are always satisfied. Using only first order regularity I showed that the 
          control to state operator is twice Fr\'echet differentiable. 
          
\item I improved slightly the regularity of the state variable $\gamma$ from 
          $\mathring{W}^1_\infty(0,1)$ to $\sob{2-1/p}{p}{0,1}$ and exploit this to show the existence of an
          optimal control together with second order sufficient optimality conditions and also gave an extension 
          to replace the simplified expression for curvature of $\Gamma_{\textrm{free}}$ in \eqref{eq:fbp_strong} by
          total curvature $H =  ( \frac{\gamma'(x)}{\sqrt{1+|{\gamma}'(x)|^2}} )'$.
\end{itemize}
%
Having developed a variational framework for \eqref{eq:fbp_strong}-\eqref{eq:fbp_cost}, it is crucial to study
FEM analysis for this optimal control problem. This is further necessary for the success of Stokes FBP in 
Subsection~\ref{subsec:stokes}, optimal control for biomembranes in Subsection~\ref{subsec:biomembranes}
and polymer extrusion process in Subsection~\ref{subsec:extrusion}. The key challenges 
for the FBP \eqref{eq:fbp_strong} are: the data smallness condition in \eqref{eq:small_data_g}, treatment of state 
and gradient constraints \eqref{eq:SS_state_const},  development of a posteriori error estimates for AFEM
and multigrid solvers. AFEM will help in capturing the surface tension effects with the help of adaptive mesh refinement
on the interface $\Gamma_{\textrm{free}}$. Furthermore, multigrid will lead to a faster solver for our
discrete nonlinear system. Finally, we need to develop optimization techniques to solve \eqref{eq:fbp_cost}.
I will carry out the following necessary steps to resolve the above mentioned issues:
%
\begin{itemize}
 \item
Examine FEM, in the spirit of \cite{PSaavedra_RScott_1991}, and derive a priori
and a posteriori error estimates for the state variables and controls 
\cite{NArada_ECasas_FTroeltzsch_2002a,MHinze_RPinnau_MUlbrich_SUlbrich_2009a}. The a priori error estimates 
will require second order sufficient conditions developed in \cite{HAntil_RHNochetto_PSodre_2012a}.
%and our preliminary work shows optimal order of convergence.
Of special interest is goal-oriented error adaptivity for optimal control problems with control
constraints \cite{MHintermuller_RHWHoppe_2008a,BVexler_WWollner_2008a} and state and gradient constraints 
\cite{MHintermuller_RHWHoppe_2010a, MHintermuller_MHinze_RHWHoppe_2010a}. 
I am interested in the goal-oriented dual weighted approach applied to 
shape optimization problems 
\cite{ABonito_RHNochetto_MSPauletti_2011a,GDogan_PMorin_RHNochetto_MVerani_2007a,RHNochetto_AVeeser_MVerani_2009a}.
\looseness=-1


\item Consider 
  time-dependent (parabolic) problems
  \cite{KKunisch_BVexler_2007a,INeitzel_BVexler_2012,FTroltzsch_2010a}. 
This will allow me to model
  the effects of applying a control over a particular time interval
  opening the door towards a stable feedback controller.         
  
\item Remove data smallness condition in \eqref{eq:small_data_g} by combining \eqref{eq:fbp_strong} with 
          \eqref{eq:SS_state_const} to get a nonlinear variational inequality (VI). 
          I intend to show the existence of solution to such nonlinear VIs. For VIs arising from standard obstacle 
          problems of type $|\gamma| \le \psi$, see\cite{HBrezis_GStampacchia_1968a,HBrezis_1972a,HBrezis_DKinderlehrer_1973a,DKindelehrer_GStampacchia_1980},
          but here I have to deal with constraint on the gradient as well i.e., $|\gamma'| \le \phi$. Here $\psi$, $\phi$ are
          given obstacles, for example in \eqref{eq:SS_state_const}, $\psi = \phi = 1$.
          

\item {\bf AFEM for VIs}: Develop AFEM for nonlinear VIs where I have to deal with obstacles of gradient type 
         \eqref{eq:SS_state_const}. 
         %VIs arising from standard obstacles of type $|\gamma| \le \psi$, see
          I point out that in literature for adaptive finite elements  for VIs\cite{KSMoon_RHNochetto_TPetersdorff_CSZhang_2007a,RHNochetto_TPetersdorff_CSZhang_2010a,
          FFierro_AVeeser_2003a,DBraess_2005a,RHNochetto_ASchmidt_KGSiebert_2006a,
          RHNochetto_KGSiebert_AVeeser_2003a,AVeeser_2001a}
           only standard obstacles of type $|\gamma| \le \psi$ have been dealt with. 
          Next I will prove the contraction property for AFEM. I refer to \cite{RHNochetto_KSiebert_AVeeser_2009a} 
          for contraction of AFEM for standard elliptic equations.

\item                
         {\bf Semi-smooth Newton method}: Study the optimal control problem with the VIs or state and gradient
         \eqref{eq:SS_state_const} constraints 
         \cite{MHintermuller_KKunisch_2006a, MHintermuller_KKunisch_2009a, JCDReyes_MHintermuller_2011a}
         and develop algorithm to solve them. 
         This requires extending the path following methods for primal-dual active set strategy or, in many cases 
         equivalently semi-smooth Newton methods \cite{MHintermuller_KIto_KKunisch_2002a}. 
         The advantages of using semi-smooth Newton methods is that 
         it converges super-linearly \cite{MHintermuller_KIto_KKunisch_2002a} and is mesh independent 
         \cite{MHintermuller_MUlbrich_2004a,MHintermuller_FTroltzsch_IYousept_2008a}, unlike the traditional 
         approaches including relaxation and augmented Lagrangian \cite{RGlowinski_1984a} which 
         typically are of first order.  

\item
{\bf Multigrid and semi-smooth Newton method}:         
         In \cite{HAntil_RHoppe_CLinsenmann_2007a, HAntil_2009a} I developed a path following interior-point method 
         for shape optimization problems where the barrier parameter is chosen adaptively, then in 
         \cite{HAntil_RHoppe_CLinsenmann_2009a}
          I gave a multilevel version of this algorithm. I plan to develop multilevel version for the 
          semi-smooth Newton methods and the path following multilevel methods for semi-smooth Newton methods.

\end{itemize}          
The AFEM for VIs, semi-smooth Newton methods and multigrid presented here will be the stepping stone for the entire remaining 
project. Next I will present the extension of ideas to Stokes FBP with constant viscosity, for a discussion of 
Stokes FBP with variable viscosity I refer to Subsection~\ref{subsec:extrusion}.
         
%\item As pointed in \cite{MHintermuller_IKopacka_2009a} sometimes a variational inequality can be written
%         as a complementarity problem, with this new formulation in hand one has to deal with optimal control
%         problem with complementarity constraints, and are referred to as mathematical programs with 
%         complementarity constraints (MPCC). There are four different kind of stationarity concepts for MPCC,
%         I will avoid this unnecessary complication in this section, but will have to deal with these issues when
%         studying EWOD in Section~\ref{sec:EWOD} where formulation does include a complementarity constraint.
         
          
          
%          The sufficient conditions
%          are further useful in deriving the a-priori error estimates which is a subject of 
%discussion in \cite{}.
%The problem described above is relatively simple, but it captures the essential features 
%associated with surface tension effects, and allows us to develop a complete second order analysis, 
%based on \cite{FTroltzsch_2010a}, is absent in the existing literature on FBP.


