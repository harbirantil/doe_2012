%!TEX root = proposal.tex

%-----------%
\section{Surface Acoustic Wave Driven Microfluidic Biochip} \label{sec:biochip}
%-----------%
An important example of a ``lab-on-a-chip'' is the microfluidic biochip (cf. Fig. \ref{f:biochip} (left)).
Microfluidic biochips are used in pharmaceutical, medical, and
forensic applications for high throughput screening, genotyping, and
sequencing in genomics, protein profiling in proteomics, and
cytometry in cell analysis \cite{JPollard_BCastrodale_2003a,HMShapiro_2003a}. They
provide a much better sensitivity and a greater flexibility than
traditional approaches. More importantly, they give rise to a
significant speed-up of the hybridization processes and allow the in-situ investigation
 of these processes at an extremely high time resolution. This can be
achieved by integrating the fluidics on top of the chip by means of a
lithographically produced network of channels and reservoirs (cf.
Fig. \ref{f:biochip} (left)).
%
\begin{figure}[htp]
   \centering
     \includegraphics[height = 3.0cm,width=4.0cm]{figures/Chip_komplett}
     \qquad \qquad
     \includegraphics[height = 3.0cm,width=4.0cm,angle=0]{figures/Jet}
%   \vspace*{0.3cm}
   \caption{Microfluidic biochip (left) and sharp jet created by
   surface acoustic waves (right)}
   \label{f:biochip}
\end{figure}
%
The idea is to inject a DNA or protein containing probe and to
transport it in the fluid to a reservoir where a chemical analysis
is performed. The fluid flow can be taken care of by external pumps
which, however, do not guarantee a very precise control of the fluid
flow and are subject to wear. A new generation of
biochips is based on a surface acoustic waves (SAW)-driven fluid
flow 
\cite{TAFranke_AWixforth_2008a,ZGuttenberg_2005a,AWixforth_2006a,AWixforth2002a,LYYeo_JRFriend_2009a}. 
Surface acoustic waves are generated
by interdigital transducers (IDT), well-known from Micro-Electro-Mechanical
Systems (MEMS). They propagate through the base of the
device with amplitudes in the range of nanometers and enter the
fluid-filled microchannels creating sharp jets (see Fig.
\ref{f:biochip} (right)). This happens within nanoseconds. In the
microchannels, the SAW get significantly damped so that an almost stationary fluid
pattern emerges which is called acoustic streaming. This relaxation process occurs on a time scale of milliseconds. I 
am thus faced with a multiscale, multiphysics problem whose
mathematical modeling and numerical simulation represents a
significant challenge. 

It is also a challenging problem with regard
to various optimization issues such as the optimal design of the
microchannels in order to achieve a maximum pumping rate. Another
one is the design of pressure driven capillary barriers between the
channels and the reservoirs (see Fig. \ref{f:biochip} (left)) to guarantee a precise filling of the
reservoirs with the probes. This
amounts to the solution of a shape optimization problem where the
mathematical model for the acoustic streaming consists of the linearized
equations of piezoelectricity \cite{AGantner_RHWHoppe_DKoster_KSiebert_2007a,HAntil_AGantner_RHWHoppe_2008a} and the 
compressible Navier-Stokes equations \cite{HAntil_AGantner_RHWHoppe_2008a,DKoseter_2007a,HAntil_MHeinkenschlos_RHoppe_CLinsenmann_AWixforth_2010d, HAntil_2009a,  HAntil_RGlowinski_RHWHoppe_CLinsenmann_TWPan_AWixforth_2010a,
HAntil_MHeinkenschloss_RHoppe_2010c}. 
%
%\begin{figure}[htp]
%   \centering
%     \includegraphics[height = 3.0cm,width=4.0cm]{figures/Kapillarstop2}
%     \qquad
%     \includegraphics[height = 3.0cm,width=4.0cm]{figures/Kapillarstop3}
%%   \vspace*{0.3cm}
%   \caption{Capillary barriers}
%   \label{fig:CapBarriers}
%\end{figure}
%
The underlying Navier-Stokes flow can be treated as either 
(i) closed fluid channel (ii) a free boundary problem.
%
In \cite{HAntil_AGantner_RHWHoppe_2008a, HAntil_MHeinkenschlos_RHoppe_CLinsenmann_AWixforth_2010d, HAntil_2009a, HAntil_RGlowinski_RHWHoppe_CLinsenmann_TWPan_AWixforth_2010a}
I was interested in reduced order modeling simulation and shape optimization of channels where the fluid
flow was considered in a closed channel. 

But some biochip models do not employ closed fluid channels but instead free capillary fluid boundaries. These 
biochips are constructed with a virtual channel geometry defined with planar regions with different wetting properties. 
The curvature and surface tension effects play a role, for  more details I refer to 
\cite{KChakrabarty_JZeng_2005a, AWixforth_2004a}. The underlying fluid model is the compressible Navier-Stokes
%
  \begin{align} \label{eq:biochip}
  \begin{aligned}
   \rho \frac{D\bv}{Dt} + \divg \bsigma
&= {\bf 0}  &&   \Omega(t) ,  \\
    \partial_t \rho +  \divg{ \rho \bv}
&= 0  &&   \Omega(t),    \\
   \bv 
&=  \partial_t \bu &&  \Gamma_{\textrm{inlet}}(t) ,        \\
 \bv \cdot {\bnu}
 &= 0       ,   \qquad  \bsigma {\bnu}
 = s H {\bnu}
 &&   \Gamma_{\textrm{free}}(t) ,   \\
   \bv(\cdot,0) = \bv_0 , & \quad  p(\cdot,0) = p_0   &&    \Omega(0),
  \end{aligned}
\end{align}
where 
%\[
$\bsigma = 2 \eta \bepsilon({\bv}) - p I 
               + \left(\zeta - \frac{2 \eta}{3}\right) \divg{\bv} I $,
%\]
$\frac{D}{Dt} = \del{ \partial_t + \bv \cdot \nabla }$, is the material time derivative 
and $ \bu $ in \eqref{eq:biochip} stands for the deflection of the walls of the microchannels caused by the SAW. 
%I note that $ \bu $ can be computed by the solution of the linearized equations of piezoelectricity, for more details see \cite{HAntil_AGantner_RHWHoppe_2008a}. 
$\bv$, $p$ denotes the fluid velocity and pressure respectively
and $\bv_0$, $p_0$ denotes the given initial data. Furthermore,
$\rho$ is the fluid density and related to pressure by the following constitutive relation:
\[
     p - p_0 =  a (\rho - \rho_0)^\gamma
\]
where $p_0$, $\rho_0$ are the equilibrium states and $a, \gamma > 0$ are constants. Finally $\eta, \zeta$ are the standard and 
bulk viscosity coefficients. $\Gamma_{\textrm{inlet}}$ is the Dirichlet boundary and $\Gamma_{\textrm{free}}$ denotes the free boundary
with capillary boundary conditions with $s$ denoting the surface tension coefficient and $H$ is the mean curvature.

The difference between \eqref{eq:biochip} and \eqref{eq:stokes}-\eqref{capillary}, is that 
\eqref{eq:biochip} is instationary compressible Navier-Stokes FBP, whereas 
\eqref{eq:stokes}-\eqref{capillary}
is incompressible stationary Stokes FBP. 
%Therefore \eqref{eq:biochip} can be thought of as an extension to  \eqref{eq:stokes}-\eqref{capillary}. 
The optimal control problem for this microfluidic biochip can be setup 
 in a similar manner as for  \eqref{eq:stokes}-\eqref{capillary}. The key challenge here is deriving the variational
 system for \eqref{eq:biochip} and proving the existence of solution to both continuous and discrete problems. 
For existence of solution to incompressible ($\divg{\bv} = 0$) Navier-Stokes FBP in H\"older spaces see
\cite{VASolonnikov_1995a, BSchweizer_1997a}, but
these results are not useful for us because of non-conformity with finite element analysis. However, no such complete 
result exists for compressible systems with slip boundary conditions
$\Gamma_{\textrm{free}}$ in \eqref{eq:biochip}, for a similar problem with \emph{fixed} domain I refer to 
\cite{RVodak_2010a}. Another compressible Navier-Stokes FBP arising due to obstacle on pressure 
(not the boundary conditions of type $\Gamma_{\textrm{free}}$) is presented in \cite{PLLions_NMasmoudi_1999a}.
%
I intend to 
\begin{itemize} 
 \item Extend the variational framework developed for incompressible Stokes FBP ($\divg{\bv} = 0$) in 
           Subsection~\ref{subsec:stokes} to compressible 
           Navier-Stokes  FBP \eqref{eq:biochip} and prove the existence of solution both in continuous and discrete 
           setting. 
 \item Extend the a priori and a posteriori error estimates from incompressible Stokes to compressible Navier-Stokes.
 \item Study the optimal control problem with \eqref{eq:biochip} as constraints
           and $\bu$ as control variable and develop AFEM, multigrid and 
           semi-smooth Newton techniques to solve the optimization problem.
\end{itemize}
The success of this project will have significant impact in forensic, pharmaceutical, and medical 
 applications for high throughput screening, genotyping, and
sequencing in genomics, protein profiling in proteomics, and
cytometry in cell analysis \cite{JPollard_BCastrodale_2003a,HMShapiro_2003a}.

Next I plan to study optimal control and shape optimization of yet another ``lab-on-a-chip" device, the so-called
electrowetting on dielectric (EWOD), where I have to deal with a time dependent Hele-Shaw FBP including a complementarity constraint to capture the contact line pinning effect. Finally, I will present the reverse EWOD 
effect to generate power.






