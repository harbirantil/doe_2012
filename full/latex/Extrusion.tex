%!TEX root = proposal.tex

%------------%
%\section{Extrusion Problem and Biomembranes}
%------------%

%-----------------%
\subsection{Stokes with Surface Tension} \label{subsec:stokes}
%-----------------%
I consider a configuration similar to the previous problem, with
$y=(\bv,p)$ being the solution of the incompressible $(\divg{\bv} = 0)$ 
Stokes equations in stationary form
\begin{equation} \label{eq:stokes}
     - \divg \bsigma = 0 ,  \qquad
                                     \divg \bv = 0 \quad {\rm in~}\Omega
\end{equation}                                     
where $(\bv,p)$ are the velocity and pressure and
$\bsigma = 2 \eta \bepsilon(\bv) - pI$ is the stress tensor and
$\bepsilon(\bv)=(\nabla \bv + \nabla \bv^T)/2$ is the strain tensor and $\eta = constant$ is the viscosity,
for a discussion on Stokes equations with variable viscosity I refer to Subsection~\ref{subsec:extrusion}.
I consider capillary boundary condition
\cite{GPGaldi_2011a,HDVBeirao_2004a,MOrlt_AMSandig_1995a,EBansch_2001a,PLLions_1996a} on 
$\Gamma_{\textrm{free}}$
with outward unit normal $\bnu$ and tangent $\btau$:
%
\begin{equation}  \label{capillary}
    {\bv} \cdot {\bnu} = 0, 
    \qquad
    \bsigma {\bnu} = s H {\bnu}
\end{equation}
%
which further read as
%
\begin{equation*}
\bv \cdot \bnu = 0, 
\qquad
{\bnu \cdot\bsigma\bnu} = s H,
\qquad
{\btau}\cdot\sigma{\bnu} = 0 .
\end{equation*}
%
The variational formulation, developing AFEM, multigrid solvers and optimization techniques 
for \eqref{eq:stokes}-\eqref{capillary}, is crucial for the optimal control of
biomembranes in Subsection~\ref{subsec:biomembranes} and removing the die swell anomaly 
in the extrusion process of polymers in Subsection~\ref{subsec:extrusion}. The PDE model for biomembranes is same as Stokes
except a fourth order boundary condition. For extrusion problem the PDE model is same as 
\eqref{eq:stokes}-\eqref{capillary}, except the non constant viscosity. 
The key challenges  for the FBP \eqref{eq:stokes}-\eqref{capillary} are:
deriving the variational formulation, proving the existence of solution. Next proving the 
existence of solution to the discrete problem and deriving the a priori error and a posteriori error estimates
and building the multi grid solvers. Finally, setup the optimization problem and develop solvers.

I intend to carry out the following crucial steps:
\begin{itemize}
\item First formulate the problem \eqref{eq:stokes}-\eqref{capillary} variationally, in the spirit of \cite{PSaavedra_RScott_1991}. 
          This has been a pending question since the paper by Saavedra and Scott \cite{PSaavedra_RScott_1991} first came out in '91.
          The main roadblock is that if $\gamma$ is assumed only a $\mathring{W}^1_{\infty}$ function, then
          it is not possible to extend the normal vector from the boundary $\Gamma_{\textrm{free}}$ to bulk $\Omega$. 
          But as I pointed out in previous section I managed to improve the regularity of $\gamma$ to 
          $\sob{2-1/p}{p}{0,1}, p>2$
          for system \eqref{eq:fbp_strong} with full curvature, this allows the required extension.
          
\item Show the existence of solution to the continuous and discrete Stokes FBP \eqref{eq:stokes}-\eqref{capillary}. 
          This requires extending Meyers \cite{NGMeyers_1963} argument and the pointwise estimates 
          \cite{VGirault_RHNochetto_Scott_2005,JGuzman_DLeykekhman_2012a} for the Stokes system. The FEM error analysis 
          will lead to a priori error estimates. Next I will develop the a posteriori error estimates. 
                
          
\item I will next examine an optimal control problem as described before for \eqref{eq:fbp_strong} using the multigrid semi-smooth
          Newton with AFEM (see Subsection~\ref{subsec:ss});  I refer to 
          \cite{MDGunzburger_HKim_SManservisi_2000a, MDGunzburger_2003a} for optimal control problems with Stokes constraints.
\end{itemize}
The success of this proposal is crucial for the extrusion process presented in Subsection~\ref{subsec:extrusion}
and optimal control for biomembranes in next subsection. 
The fluid model for biomembranes 
is incompressible Stokes equations \eqref{eq:stokes} but the membrane effects are modeled by a fourth order condition 
on the free boundary instead of second order curvature in \eqref{capillary}.

%-----------------------------------------------------------------------------
%\medskip\noindent
\subsection{Stokes with Bending Elasticity: Biomembranes} \label{subsec:biomembranes}
%----------------------------------------------------------------------------
%
Lipid molecules when immersed in aqueous medium leads to an aggregation into a bilayer or
membrane. Studying these membranes is further  necessary
to understand the real cells in biosciences. These cells can be manipulated 
by applying external forces, for example, treatment of cancer and drug delivery. 
The goal of this subsection is to control the shape of biomembranes,
whose equilibrium shape can be modeled by minimization 
of bending (or Willmore) energy $W(\Gamma) = \frac12 \int_\Gamma H^2$
subject to area and volume constraints (Helfrich model) 
\cite{ABonito_RHNochetto_MSPauletti_2010a,ABonito_RHNochetto_MSPauletti_2011a},
where $\Gamma$ denote the free boundary.
The shape derivative of $W(\Gamma)$  \cite{JSokolowski_JPZolesio_1992,MDelfour_JZolesio2011} in the direction $\bw$ is
%
\begin{equation}\label{willmore}
\delta W(\Gamma;\bw) = \int_\Gamma \Big( -\Delta_\Gamma H -\frac12 H^3
+ 2 H\kappa\Big) \bw\cdot\bnu,
\end{equation}
where $H$ is the total curvature and $\kappa$ is
the Gauss curvature, which is a fully nonlinear operator.
Furthermore, $\bnu$ is the outward unit normal, and $\Delta_\Gamma$ denotes the Laplace-Beltrami operator.
%
%based on a model presented in \cite{ABonito_RHNochetto_MSPauletti_2010a,ABonito_RHNochetto_MSPauletti_2011a},
%where consider geometric biomembranes governed by an $L^2$-gradient flow
%  for bending (or Willmore) energy $W(\Gamma) = \frac12 \int_\Gamma H^2$
%  subject to area and volume constraints (Helfrich model). Here $\Gamma$ denote the free 
%  boundary.
%  The shape derivative of $W(\Gamma)$ in the direction $\bw$ is
%%
%\begin{equation}\label{willmore}
%\delta W(\Gamma;\bw) = \int_\Gamma \Big( -\Delta_\Gamma H -\frac12 H^3
%+ 2 H\kappa\Big) \bw\cdot\bnu,
%\end{equation}
%%
%  where $H$ is the total curvature and $\kappa$ is
%  the Gauss curvature, which is a fully nonlinear operator.
%  They next give a  variational formulation, based on shape differential
%  calculus \cite{JSokolowski_JPZolesio_1992,MDelfour_JZolesio2011},
%%
%\begin{equation}\label{willmore-variational}
%\delta W(\Gamma;\vect{\phi}) =
%\int_\Gamma \sgrad{\vect{\phi}} \cdot \sgrad\bH
% -\int_\Gamma (\sgrad{\bX}+\trans{\sgrad \bX})
%  \sgrad{\vect{\phi}}:\sgrad{\bH}
%+\frac 1 2\int_\Gamma \sdiv{\bH} \sdiv{\vect{\phi}},
%\end{equation}
%%
%  and corresponding discretization via parametric FEM using
%  quadratic isoparametric elements and a semi-implicit Euler method 
%  \cite{ABonito_RHNochetto_MSPauletti_2010a}. Here $\bH$ and $\bX$ 
%  denote the 
%  vector curvature and the position vector respectively and are related as
%  $ \bH = - \lapl_\Gamma \bX$.
%  We also document its performance with a number of
%  simulations leading to dumbbell, red blood cell and toroidal
%  equilibrium shapes while exhibiting large deformations.
%In \cite{ABonito_RHNochetto_MSPauletti_2011a} they consider fluid-membrane interactions 
%The incompressible fluid is governed
%by the Navier-Stokes equation and the membrane exerts a force $\delta
%W(\Gamma)$ on the boundary $\Gamma$ that balances the normal traction.
Next I will describe a fluid-membrane interaction model and the key steps to setup the
optimal control problem for biomembranes. The biomembrane optimal control problem differs from
the Stokes optimal control problem in Subsection~\ref{subsec:stokes} only in terms of the boundary condition \eqref{capillary}.
%This is the crucial issue that need to be taken into account for the success of this subsection.

I intend to
\begin{itemize}
 \item Model the fluid using the incompressible Stokes equations \eqref{eq:stokes},
           and will next consider fluid-membrane interactions where the membrane exerts a force $\delta W(\Gamma)$
           on the boundary $\Gamma$ that balances the normal traction $\bsigma{\bnu}$. This will be achieved by
           considering \eqref{willmore} instead of \eqref{capillary}  and
           by setting $\bsigma{\bnu} = \delta W(\Gamma)$, which is a fourth order condition, instead of the second order
           condition in \eqref{capillary}.
           
\item Derive a variational form for \eqref{willmore} (see also \cite{ABonito_RHNochetto_MSPauletti_2010a}),
          and next prove the existence  of solution for the resulting system.
%          Consider fluid-membrane interactions where the membrane exerts a force $\delta W(\Gamma)$
%          on the boundary $\Gamma$ that balances the normal traction. This will be achieved by
%          considering \eqref{willmore} instead of \eqref{capillary}  and
%          by setting $\bsigma{\bnu} = \delta W(\Gamma)$, which is a fourth order condition. 
          %For a somewhat similar discussion with Navier-Stokes equations in the bulk I refer to 
          %         \cite{ABonito_RHNochetto_MSPauletti_2011a}. 
          %The novelty of our approach is in deriving the 
          %variational formulation for the entire system and proving the existence of solution based on our
          %work in previous section for Stokes equations. 
          
\item Finite element methods for the Willmore flow of graphs has been studied
          by Deckelnick and Dziuk \cite{KDeckelnick_GDziuk_2006a}. I plan to extend this study to the
          coupled problem with Stokes, derive a priori and a posteriori error
          bounds, and next examine the corresponding optimal control problem.
          
\end{itemize}          
The success of this project will have significant impact in biosciences. 
My plan next to extend the discussion from Subsection~\ref{subsec:stokes}, where I considered 
Stokes FBP with constant viscosity to Stokes FBP with variable viscosity, the fluid is still
incompressible but is non-Newtonian (variable viscosity). 

%------------%
\subsection{Stokes and Extrusion Problem} \label{subsec:extrusion}
%------------%
In polymer extrusion of synthetic yarns, the solid polymer is heated beyond the melting point is to 
be enough malleable. Then, the material is forced by one or more screws through a special die 
to produce a continuous manufactured item.
%(see Figure~\ref{fig:extruder}). 
With such a manufacturing process, it is possible to obtain, for example, 
sheets, films, pipes, sections, layers and slabs. 
%
%\begin{figure}[htp]
%\centering
%\includegraphics[width=6cm]{./FIGURES/extruder}
%\caption{Extrusion process outline.} 
%\label{fig:extruder}
%\end{figure}
%
%It is known that the cavity affects the fiber insulation property and it improves the 
%fiber permeability, volume and pleating, while reducing the fiber pilling.
%I will focus on Nylon Polyamide~6~(PA6) yarns because it is the most employed synthetic polymer 
%within the textile industry.
The main problem linked to polymer extrusion is:
%
\begin{itemize}
  \item \emph{die-swell} phenomenon which is the increase of polymer section when the polymer leaves the 
           die (see Figure~\ref{fig:die_swell_dominio}, left). 
           \emph{Die-swell} is common among polymeric materials and it is linked to their visco-plastic behavior
           \cite{RI_Tanner_2003a,HXHuang_YSMilao_DLi_2006a}. 
           \begin{figure}[htp]
              \centering
              \includegraphics[width=5.0cm]{./FIGURES/my_die_swell} \qquad
              \includegraphics[width=5.0cm]{./FIGURES/free_boundary}
              \caption{\emph{Die-swell} phenomenon (left), 2D sketch of the boundary conditions (right).}
              \label{fig:die_swell_dominio}
            \end{figure}
\end{itemize}            


%-----------%
%\subsubsection{Mathematical model}\label{sec:mathematical_modelling}
\medskip\noindent
{\bf A Model for Extrusion Problem.}
%-----------%
In the following, I will briefly discuss a mathematical model 
\cite{PFAntonietti_PFFadel_MVerani_2011a,JWBarrett_SChristoph_ESuli_2005a,JWBarrett_ESuli_2007a,EMitsoulis_1999a,EMitsoulis_2007a} of the extrusion process. 
Let $\Omega\subset\RR^d$, $d=2,3$ be the computational domain. 
Let us consider $\Omega_1,\Omega_2\subset\Omega$ such that $\Omega_2=\Omega\setminus\overline{\Omega}_1$.    
The region $\Omega_1$ includes the extrusion die, while $\Omega_2$ includes the free surface
(see Figure~\ref{fig:die_swell_dominio}, right).
%
%\begin{figure}[htp]
%\centering
%\includegraphics[width=6cm]{./FIGURES/free_boundary}
%\caption{2D sketch of the boundary conditions.}
%\label{fig:dominio}
%\end{figure}
%
I will consider Polyamide~6~(PA6) polymer whose flow can be modeled as non-Newtonian, incompressible, steady and 
isothermal \cite{PFAntonietti_PFFadel_MVerani_2011a}. 
Thus, the stationary extrusion process is described by the Stokes equations
\eqref{eq:stokes} from Subsection~\ref{subsec:stokes},
with two key differences (i) the viscosity $\eta$ is not a constant (ii) and instead of $\bsigma \bnu = s H \bnu$ in
\eqref{capillary} I have $\bsigma \bnu = {\bf 0}$. In other words the 
free boundary problem is: 
find the free surface $\Gamma_{\textrm{free}}$, the velocity $\bv$ and the pressure $p$ such that 
%
\begin{align}  \label{freepb_extrusion}
\begin{aligned}
                          - \divg \bsigma &= {\bf 0} ,  \quad   \divg \bv = 0 \quad {\rm in~}\Omega, \\
                                               \bv &= \bg \ \quad {\rm on~} \Gamma_{\textrm{inlet}} \cup \Gamma_{\textrm{wall}},
                                                             && \bsigma \bnu = {\bf 0} \quad{\rm on~} \Gamma_{\textrm{out}},    \\
                            \bv \cdot \bnu &= 0, \quad 
                             \bsigma \bnu = {\bf 0} \quad{\rm on~}  \Gamma_{\textrm{free}}, 
\end{aligned}
\end{align}
%
where $\bnu$ is the outer normal vector, $\bsigma = 2 \eta(\dot{\gamma}) \bepsilon(\bv) - p I$ is the stress tensor, 
$\bepsilon(\bv)=(\nabla \bv + \nabla \bv^T)/2$ is the strain tensor, 
 $\dot{\gamma}=\sqrt{2\bepsilon(\bv):\bepsilon(\bv)}$ is the shear rate, $\eta$ is the viscosity 
and $\bg$ is defined as: $\bg = \bv_{\textrm{inlet}} \textrm{ on } \Gamma_{\textrm{inlet}}$
and $\bg = {\bf 0} \textrm{ on } \Gamma_{\textrm{wall}}$.
%
%\begin{equation*}
%  g=\begin{cases}
%  u_{\textrm{inlet}}&\textrm{on } \Gamma_{\textrm{inlet}} \\%u_{\textrm{\textrm{inlet}}} &\textrm{on } \Gamma_{\textrm{inlet}}\\
%  0 & \textrm{on }\Gamma_{\textrm{wall}}.
%  \end{cases}
%\end{equation*}
%
As a consequence of the non-Newtonian nature of the Polyamide~6, the viscosity $\eta(\cdot)$ 
depends on the shear rate $\dot{\gamma}$. The most common models in literature are the following
\cite{PFAntonietti_PFFadel_MVerani_2011a}:
\begin{align} \label{eq:2.7}
\begin{aligned}
\eta(\dot{\gamma})&=\textrm{constant} \quad 
           \mbox{(Newtonian model)};   \\
\eta(\dot{\gamma})&=\eta_{\infty} + \frac{\eta_{0}-\eta_{\infty}}{\left( 1 + \lambda^2 \dot{\gamma}^2\right)^{(1-n)/2}} \quad \mbox{(Bird-Carreau  model)};  \\
 \eta(\dot{\gamma})&=\eta_{\infty} + \frac{\eta_{0}-\eta_{\infty}}{1 + (\lambda \dot{\gamma})^n} \quad 
                 \mbox{(Cross model)}  ,
\end{aligned}
\end{align}
%
where $\lambda$ is the characteristic time, 
%$n$ the pseudo-plastic index, 
$\eta_{0}$ and $\eta_{\infty}$ are the viscosities of the two Newtonian plateau i.e., viscosity at 
zero shear rate and at infinite shear rate respectively. Furthermore, $n$ denotes a dimensionless constant.
The momentum equation is then highly coupled with the viscosity constitutive equation.

The key challenges, those haven't been resolved already in 
Subsection~\ref{subsec:stokes}, associated with FBP \eqref{freepb_extrusion} is the presence of non constant viscosity and the absence
of curvature from the free boundary (compare with Newtonian Stokes in \eqref{eq:stokes}-\eqref{capillary}).
It is well known that curvature introduce smoothing effects \cite{DCoutand_SShkoller_2007a}.
The second challenge associated with \eqref{freepb_extrusion} is setting the proper optimization problem to rectify the 
die-swell anomaly. 
%
The steps I will take to resolve these issues are:
%
\begin{itemize}
 \item First study \eqref{freepb_extrusion} with the boundary condition \eqref{capillary} so 
           that we are close to the setting for Stokes equations in Subsection~\ref{subsec:stokes} , but with the 
           viscosity given by the cross model in \eqref{eq:2.7} which is justified in \cite{PFAntonietti_PFFadel_MVerani_2011a}. 
           %It is important to note that the presence of curvature introduces a smoothing effect on the boundary 
           %\cite{DCoutand_SShkoller_2007a}. 
           I will next prove the existence of solution to this model in variational 
           form, followed by the original system \eqref{freepb_extrusion} and its variational form. 
                     
 \item Rectify {\it die-swell} anomaly by solving a minimization problem to attain a desired configuration for example 
          $\gamma_d$ in \eqref{eq:fbp_cost} with 
          a corresponding FBP \eqref{freepb_extrusion} as constraints using the adaptive multilevel semi-smooth Newton 
          developed for Stokes in Subsection~\ref{subsec:stokes} and will extend the a posteriori error estimates already 
          derived in previous work on Stokes in Subsection~\ref{subsec:stokes} to this model.
 
\end{itemize} 
The success of the project will have significant impact on the extrusion industry including biomass briquettes, metal, food,
textile and plastics.

Having presented the Stokes FBP for Newtonian and non-Newtonian fluid in Subsections~\ref{subsec:stokes}
and \ref{subsec:extrusion}, the plan is to next extend the ideas to unsteady compressible Navier-Stokes FBP,
in particular I am interested in optimal control of microfluidic biochips.


