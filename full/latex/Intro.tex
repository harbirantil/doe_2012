%!TEX root = proposal.tex

%----------------%
\section{Introduction}
%----------------%
Lab-on-a-chip devices like electrowetting on dielectric (EWOD), surface acoustic wave driven microfluidic biochips
and phenomenon like polymer extrusion have found tremendous applications in liquid lenses,
electronic displays, diagnostics on physiological fluids, forensics and the manufacturing of polymer products.
The governing partial differential equations (PDEs) are \emph{nonlinear}, \emph{multiscale} and typically
the domain is also an unknown; they are \emph{free boundary problems} (FBP). 
The goal of this project is to model and control such phenomenon and to design, test, and analyze reliable and efficient 
methods to solve minimization problems with such PDEs as constraints.
Some of the challenging tasks are: The PDEs involved are FBP. 
The state constraints lead to either non-smooth cost functionals and/or variational inequalities. The existence of solution 
to the underlying PDEs can only be proved in 
certain H\"older spaces, which does not conform with the finite element method (FEM). Furthermore, adaptive finite element methods 
(AFEM) and multilevel  solvers are to be designed and at times the models are debatable. 
I propose a bottom-up approach, at the first stage I will develop a variational framework for the underlying PDEs in the continuous
setting. Next a posteriori error estimates (for AFEM) and multilevel solvers will be developed for PDEs. Finally, I will prove
the existence of solution to the minimization problems and will develop AFEM and multilevel solvers for the entire minimization 
problem.
Rigorous analysis of both model and algorithms will lead to improving the fidelity and predictability of these complex systems.
%For example, the \emph{contact-line pinning} effect in EWOD \cite{SWalker_BShapiro_RNochetto_2009a} or removing the 
%\emph{die-swell} anomaly in polymer extrusion \cite{PFAntonietti_PFFadel_MVerani_2011a}. Finally, the 
%control of droplets will help in generation of power via {\it reverse} EWOD \cite{TKrupenkin_JATaylor_2011a} for
%mobile devices.

The PDEs modeling polymer extrusion (see Section~\ref{subsec:extrusion}),  microfluidic biochips (see Section~\ref{sec:biochip})
and EWOD (see Section~\ref{sec:EWOD}) have one important common feature i.e., the curvature and 
surface tension effects dictate the bulk processes. 
Due to such bulk-surface interactions, the problem are multiscale in nature. For a simpler
model capturing these effects, see  \cite{HAntil_RHNochetto_PSodre_2012a}, where the state system is based on coupling the Laplace equation in the bulk $\Omegag \subset \mathbb{R}^2$
\[
    -\lapl y = 0 ,
\]
with a Young-Laplace equation on the free boundary $\Gamma_\gamma \subset \mathbb{R}^1 $
\[
    -s H + \nd y(x_1) = 0 ,
\]
to account for surface tension (for more details, see Subsection~\ref{subsec:ss}). Here $H$ is the curvature of the free boundary and 
$s$ is the surface tension coefficient. This amounts to solving a second order system both in the bulk $\Omega_\gamma$ 
and on the interface $\Gamma_\gamma$. 


Although AFEM already leads to substantial reduction of the computational costs, and can be viewed 
as sort of model reduction technique itself, in many cases the costs are still significantly high so that there is need for further 
reductions. This particularly applies to optimal control problems where the successive solution of the underlying state equation 
and the associated adjoint equation is required (see Section~\ref{sec:mr}). 
%Therefore, serious attempts have been undertaken to achieve a significant
%reduction of the computational costs based on Reduced Order Models (ROMs). 
Reduced order models determine a subspace that contains
the ``essential" (which is problem dependent) dynamics of the nonlinear evolution and/or parametrized PDEs and project
these PDEs onto the subspace. If the subspace is small, the original nonlinear PDEs in the optimization problem can be 
replaced by a small system and the resulting approximate optimization problem can be solved efficiently. 
 The most 
commonly used ROM techniques are balanced truncation model reduction, Krylov subspace methods, proper orthogonal
decomposition (POD), and the reduced basis methods (RB).
This will be the subject of discussion in Section~\ref{sec:mr}.
%Among the most 
%commonly used ROM techniques are balanced truncation model reduction, Krylov subspace methods, proper orthogonal
%decomposition (POD), and the reduced basis methods (RB) (see  \cite{HAntil_MHeinkenschlos_RHoppe_CLinsenmann_AWixforth_2010d, HAntil_2009a, HAntil_MHeinkenschloss_RHoppe_DCSorensen_2010b, HAntil_MHeinkenschloss_RHoppe_2010c, ACAntoulas_2005a, ZBai_2002a, RWFreund_2003a, ZBai_PMDewilde_RWFreund_2005a, PBenner_VMehrmann_DCSorensen_2005a, 
%MHinze_SVolkwein_2005a, SChaturantabut_DCSorensen_2010a, MBarrault_YMaday_NCNguyen_ATPatera2004a, MAGrepl_ATPatera_2005a, GRozza_ATPatera_2007a, PBinev_ACohen_RDevore_2010a}).
%I will focus on POD and RB applied to optimal control problems in Section~\ref{sec:mr}.


%Another key aspect of this proposal is model reduction where I am motivated by numerical relativity 
%(see Section~\ref{sec:grav_waves}). 
%A LIGO \cite{JAbbott_2009a,SJWaldman_2006a} search for a \emph{gravitational wave} involves comparing a detected signal (usually noisy) with 
%templates generated from an 8-dimensional parameter space. These templates are solution to Einstein's equations,
%which are parametric hyperbolic PDEs and require tremendous resources for each parameter solve. 
%A post-Newtonian (PN) approximation to Einstein equation leads to parameterized ordinary differential equations
%(ODEs) which further can be approximated by a stationary phase approximation (SPA) in inspiral regime for black holes. 
%SPA has a closed form expression and is currently being used in initial LIGO and are considered to be
%good enough at this stage. But even with this approximation, the amount of data is of such an extreme scale that a 
%single search requires weeks of computational time on supercomputers. U propose to use reduced basis method,
%which will require developing a posteriori error estimates for PN ODEs
%so that templates can be generated faster (needed for advanced LIGO). To downsample large scale data tempalates or observations, I propose to use reduced order quadratures (ROQ), recently developed in \cite{HAntil_SField_RHNochetto_MTiglio_2011a}. 
%Since the detected signals are noisy, I further propose to develop algorithms to handle such data.
%The reduced model developed will facilitate in drawing scientific conclusions in real time, without 
%compromising accuracy.

%In order to realize the ideas stated in the proposal the plan is to proceed in an ascending order from 
%Section~\ref{sec:stokes_fbp}-\ref{sec:EWOD} and work on Section~\ref{sec:mr} simultaneously. 
%In particular in Subsection~\ref{subsec:ss}, I will study optimal control of a model FBP
%starting from my pervious work in \cite{HAntil_RHNochetto_PSodre_2012a}, I propose an extension 
%using variational 
%inequalities, AFEM, multigrid methods and optimization techniques including semi-smooth Newton methods. 
%The techniques described here will be used all throughout the proposal with 
%modifications. I propose a variational framework for the Stokes FBP in Subsection~\ref{subsec:stokes}. A polymer 
%extrusion model is presented in Subsection~\ref{subsec:extrusion}, where the underlying FBP is the Stokes system. 
%Motivated by surface acoustic wave driven microfluidic biochips, I will present an extension from incompressible Stokes FBP to 
%compressible Navier-Stokes FBP in Section~\ref{sec:biochip}.
%In Section~\ref{sec:EWOD} I will study the contact line pinning effects for 
%EWOD first and then generate power for mobile devices using reverse EWOD in Subsection~\ref{subsec:REWOD}.
%Finally, I will present model reduction for optimal control problems and data reduction techniques  in Section~\ref{sec:mr}, followed by timeline of the project in Section~\ref{sec:timeline} and educational impact in 
%Section~\ref{sec:education}.





