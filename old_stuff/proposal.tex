\documentclass[11pt]{article}
\setlength{\topmargin}{-10mm}
\setlength{\textheight}{220mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\textwidth}{165mm}
\renewcommand{\baselinestretch}{1.1}

\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[small,compact]{titlesec}
\usepackage[colorlinks=true]{hyperref}

%\setlength{\parindent}{0ex}
%\setlength\textwidth{6.5in}
%\setlength\oddsidemargin{0in}
%\setlength\evensidemargin{0in}

% \parskip 0.1in % gap between paragraphs
%\parindent 0.2in  % indentation of paragraphs

\abovecaptionskip 0.05in
%\belowcaptionskip -0.1in

%\topmargin -0.5in 
%\textheight 9.5in 
%\input{latex-defs}

\input{mathdefs}
\input{projdefs}

%------------------  modifications in red  %-----------------------------
\newcommand{\HA}[1]{{\color{red}~\textsf{[HA: #1]}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%\layout
%
%
%    MACROS
\begin{center}
\textrm{\Large\bf PROJECT DESCRIPTION}
\end{center}

Given a parametric PDE with a large number, say $N$, of possible
vector-valued parameters $\mu$, it is often the case that the solution 
dependence on $\mu$ is low dimensional and smooth.
Several prominent methods exist for solving the PDE for just
$n\ll N$ parameters and still
capture the essential features of the PDE correctly and next solve for
any other values of $\mu$ fast. Among
these techniques I mention the balanced truncation model reduction
(BTMR), proper orthogonal decomposition (POD), reduced basis method (RB).
For general nonlinear
problems the most widely used techniques to reduce the complexity are
POD and RB
\cite{ACAntoulas_2005a,MBarrault_YMaday_NCNguyen_ATPatera2004a,
PBenner_VMehrmann_DCSorensen_2005a,PBinev_ACohen_RDevore_2010a,
SChaturantabut_DCSorensen_2010a,MAGrepl_ATPatera_2005a,MHinze_SVolkwein_2005a}.
Both POD and RB work on a Hilbert space setting:
POD reduces the dimensionality from $N$ to $n$ by singular value 
decomposition, whereas RB uses a greedy algorithm an a posteriori
error estimates to recursively construct the $n$ solutions. This
project is about developing and
applying POD and RB to two specific problems.


%---------------------------------------------------------------------------
\subsection{Model Reduction Methods and Applications}\label{S:model-reduction}
%---------------------------------------------------------------------------
%

%-------------------------------------------------------------------------
\medskip\noindent
{\bf Numerical General Relativity.} 
%-------------------------------------------------------------------------
%
Einstein's evolution equations, which 
are a set of quasilinear hyperbolic-elliptic equations 
\cite{AMAlekseenko_DNArnold_2003a, DNArnold_2000a, DNArnold_NTarfulea_2007a, GCalabrese_LLehner_MTiglio_2002a, GCalabrese_JPullin_OReula_OSarbach_MTiglio_2003a, OSarbach_MTiglio_2005a}. The hyperbolic sector is
usually solved in first order form, in which case they become a rather intensive system of about 
50 partial differential equations (PDE), with very large lower order terms 
\cite{EPazos_MTiglio_MDDuez_LEKidder_SATeukolsky_2009a}. 
For the
binary black hole problem the parameter space is 8-dimensional: the
masses and spins (1 and 3 degrees of freedom, respectively) for each black hole.
They constitute a formidable computational challenge even for one
parameter (in the order of $100,000$ CPU hours for a single, 
highly efficient and optimized simulation), 
thereby making the solution for many parameters a daunting
task. 
%
Still, computations are crucial to produce templates of gravitational waves that will enable, through matched filtering, their detection with laser interferometer techniques 
(LIGO \cite{LIGO} and Advanced LIGO). 
Given data $s$, the idea is to consider a tracking type objective
functional and solve the following minimization problem
\begin{align} \label{eq:min-problem}
\min_{\mu \in \mathcal{P}} \mathcal{J}(f,\mu) := \frac{1}{2} \|  s - h_{\mu}(f)  \|_2^2 , \qquad  \mbox{subject to }
          h_{\mu} \mbox{ satisfying Einstein's equations}.
\end{align}
Here we assume that $\mathcal{P}$ is a non-empty, closed convex parameter set and $f\in \Omega$ is the frequency. 
This falls under the PDE constrained optimization problems \cite{FTroltzsch_2010a}. 

The detection of gravitational waves will open a new window to the universe \cite{JAbadie_2010a}.
 As a first step forward we study Stationary phase approximations (SPA) waveforms which are an 
approximation to post-Newtonian (PN) equations in absence of spin, which are in turn approximation to 
Einstein's equations are used in LIGO and are expected to be good enough for detection 
\cite{BJOwen_BSSathyaprakash_1999a,BJOwen_1996a}. In other words we will use SPA as $h_{\mu}$ instead of 
solving the Einstein's equations in \eqref{eq:min-problem}. 
LIGO requires about 10, 000 SPA templets for matched filtering with a cost of couple of months on 
supercomputers. When reduced basis (RB) is applied to SPA waveforms the gain in computational saving 
is of several orders of magnitude without loss of accuracy \cite{MTiglio_2011a}. But still computing the 
$L^2$ inner products for all the frequencies and chirp masses is a a very challenging task. In 
\cite{HAntil_SField_RHNochetto_MTiglio_2011a} we 
propose to evaluate the inner products using RB and empirical interpolation method (EIM)
and drive a priori and a posteriori error estimates. 

More needs to be done to
improve the implementation and \emph{a posteriori error estimation} is
to be developed. We will later consider 
phenomenological waveforms \cite{LSantamaria_2010a} 
which are similar to SPA (in the sense that they are known in
closed form) with 2 more parameters - the magnitude of the spins.
This would already have a significant impact in LIGO searches for gravitational waves.
We will next examine the post-Newtonian (PN)
approximation which contains 8 parameters and can be considered as
relativistic corrections to Newton's laws.
And finally, we will tackle the effective one body model \cite{ABuonanno_TDamour_1999a}, which
is a one body approximation of a particle on the spacetime of a large
black hole, and combines PN, ringdown perturbation theory, 
and full numerical simulations to match for free parameters.
We expect to guide the NRAR (Numerical Relativity-Analytical Relativity, 
\url{https://www.ninja-project.org/doku.php?id=nrar:home}) collaboration in their effors to 
choose which are the most ``relevant'' simulations to solve for.


%------------------------------%
 \bibliographystyle{plain}
 \bibliography{references}

%------------------------------%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%