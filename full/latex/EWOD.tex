%!TEX root = proposal.tex

%------------%
\section{Control for EWOD} \label{sec:EWOD}
%------------%
The electrowetting effect is defined as the change in the solid-liquid contact angle due to applied electric field 
(see Figure~\ref{fig:EWOD}) \cite{FMugele_JCBaret_2005a, FMugele_MDuits_DVandenEnde_2010a,SWalker_BShapiro_RNochetto_2009a}.
Due to the electric field the liquid droplets can be manipulated in-place as in electronic displays and liquid lenses, and can 
be moved around as in clinical diagnostics on physiological fluids. 
In Subsection~\ref{subsec:EWOD}, I will begin with Hele-shaw equations based on  \cite{SWalker_BShapiro_RNochetto_2009a, SWalker_ABonito_RNochetto_2010a} as the underlying model, where
the initial goal will be to attain a desired shape for the droplet. The setup include droplets sandwiched between the electric
plates, and the electric field actuation or voltage acts as a control variable. In EWOD electrical energy is being converted to mechanical energy. 
This phenomenon could be reversed as pointed out in \cite{TKrupenkin_JATaylor_2011a}
i.e., the mechanical energy 
can be converted into electrical energy, this reverse phenomenon is called \emph{reverse} EWOD (REWOD), I will 
discuss this in detail in Subsection~\ref{subsec:REWOD}. 
A modified cost functional to attain a desired electric field actuation or voltage will be used in case of REWOD to generate power where instead of 
electric field as in EWOD, the shape, placement of droplets and the thickness of electrode will act as the control. 
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth]{figures/EWOD}
  \caption{\label{fig:EWOD}An example of EWOD. The darker electrode when active, decreases the surface tension 
                 of the liquid touching it, making it to move to right. Figure is a courtesy of \cite{SWalker_BShapiro_RNochetto_2009a}.}
\end{figure}

%
\subsection{Shape Optimization EWOD} \label{subsec:EWOD}
This project is about modeling and simulating parallel-plate EWOD
microdevices (see Figure~\ref{fig:EWOD}) that move fluid droplets through surface tension effects,
which are important in micro-engineering 
\cite{FMugele_JCBaret_2005a,FMugele_MDuits_DVandenEnde_2010a,SWalker_BShapiro_RNochetto_2009a,SWalker_ABonito_RNochetto_2010a,TKrupenkin_JATaylor_2011a}.
When voltage from an external source is applied between the conductive droplet and dielectric film
coated electrode, the electric and surface tension effects compete. The electric energy changes the surface tension 
which allows drops to move, split, merge, and mix fluids in microscale devices. A contact angle is an angle $\theta$
measured through the liquid, where liquid/gas interface meets the solid. The initial model of contact angle variations with
respect to voltage is the Young-Lippmann equation \cite{GLippmann_1875a}
\[
     \cos \theta = \frac{\sigma_{\textrm{sg}}-\sigma_{\textrm{sl}}+C_{\textrm{cap}} V^2/2}{\sigma_{\textrm{lg}}} ,
\]
where $\sigma_{\textrm{sg}}$ and $\sigma_{\textrm{sl}}$ are the surface tensions of the solid-gas and 
solid-liquid layers, respectively, and $C_{\textrm{cap}}$ is the capacitance per unit area of the charging underneath the 
electrode. Thus, when the voltage increases the contact angle decreases and therefore the droplet becomes flat i.e., 
the electrical energy is converted into mechanical energy. 

In real applications there are loss mechanisms such as contact angle saturation (pinning) and hysteresis (viscous-damping)
\cite{FMugele_JCBaret_2005a,SWalker_BShapiro_2006a,SWalker_BShapiro_RNochetto_2009a, TDBlake_EHStattersfield_2000a, TDBlake_YDShikhmurzaev_2002a, TDBlake_2006a, JEggers_REvans_2004a, YDShikhmurzaev_TDBlake_2004a, WRen_EWeinan_2007a}. If not
taken into account, which is the case in Young-Lippmann model the predicted speeds of droplets are ten times faster than 
observed experimentally \cite{SWalker_BShapiro_2006a}. 
I will begin with a model presented in \cite{SWalker_BShapiro_RNochetto_2009a} where both of these effects are taken into 
account, for a diffuse interface model for EWOD I refer to \cite{RHNochetto_AJSalgado_SWWalker_2011a,HAbels_HGarcke_GGruen_2010a,HLu_KGlasner_ABertozzi_CKim_2007a, TQian_XPWang_PSheng_2003a, TQian_XPWang_PSheng_2006a}. The fluid dynamics is modeled by Hele-Shaw 
type equations (including inertia $\partial_t \bv$) on a deformable domain (perhaps exhibiting splitting and reconnection):
%
\begin{align}\label{EWOD}
\begin{aligned}
\alpha {\partial_t \bv + \beta \bv} + \nabla p &= 0, && \Omega(t) \\
\divg{\bv} &= 0, && \Omega(t) \\
p &= H + E+\lambda + D_{visc} {\bv}\cdot {\bnu},  && \partial\Omega(t) \\
{\lambda} &= {P_0} \sign {(\bv\cdot\bnu)} .
\end{aligned}
\end{align}
%
The boundary condition for pressure $p$ accounts for surface tension with curvature
$H$, and electric field
actuation $E$ (Young-Lipmann), {\it contact line pinning} or sticking of
the interface $\lambda$, and viscous damping $\bv\cdot \bnu$; the latter two effects are
crucial to get the correct shapes and dynamics. Note $P_0$ is a given quantity. 
In \cite{SWalker_BShapiro_RNochetto_2009a} Walker et al presented a 
mixed method formulation which allows for a natural discretization of curvature $H$,
and studied the stability and accuracy for a time-discrete version of
\eqref{EWOD} \cite{SWalker_BShapiro_RNochetto_2009a,SWalker_ABonito_RNochetto_2010a}, as well as presented this 
phenomenological model for $\lambda$, and compared 
simulations with lab experiments: the agreement 
is particularly striking and an excellent indication that \eqref{EWOD} can be used for
design and control.  
Below I state the key issues involved in this project and ideas to fix them:
%
\begin{itemize}
 \item First prove the existence of solution to \eqref{EWOD} in the continuous case (no time-discrete)
           without the $\lambda$ equation, this is an ongoing work in 
           \cite{HAntil_MHintermuller_RHNochetto_PSodre_2012a}. I am studying an enriched model with 
           the material time derivative 
           \[
                \frac{D\bv}{Dt} = \del{\partial_t  + \bv \cdot \nabla } \bv
           \]   
           instead of simply $\partial_t \bv$.           
           Under the assumption that the free boundary can be written in graph form,
           I will show the existence of solution to \eqref{EWOD}. 
           For a discussion on Euler equations with free boundary in graph form, with surface tension effects,
            see \cite{BSchweizer_2005a}.
           Note that $(\bv \cdot \nabla)$ was neglected in  
           \cite{SWalker_BShapiro_RNochetto_2009a, SWalker_ABonito_RNochetto_2010a} because of a small
           coefficient in front when written in dimensionless form, but with material derivative the model is more accurate
           and is essential to use the techniques from 
           \cite{BSchweizer_2005a}.
           
 \item Give a variational formulation for \eqref{EWOD}, note in 
           \cite{SWalker_BShapiro_RNochetto_2009a, SWalker_ABonito_RNochetto_2010a} 
          only time discrete variational form is presented.
           
 \item Get rid of the graph assumption but consider a local graph approach for free boundary 
           and show the existence of solution to \eqref{EWOD} (without $\lambda$), in spirit of 
           \cite{JShatah_CZeng_2008a, DCoutand_SShkoller_2007a}, where they study the 
           Euler equations with surface tension effects.
%            to show the existence of solution to \eqref{EWOD} still without $\lambda$.                     
 
\item Instead of the Neumann condition \eqref{eq:fbp_strong}, I will consider the
           Dirichlet condition \eqref{EWOD} on pressure $p$, relevant to EWOD, and use the 
           voltage actuation $E$ as a control. I
           will formulate such a problem as an optimal shape design problem in
           the spirit of the prototype FBP above in Subsection~\ref{subsec:ss} and study it using the path following methods
           with  semi-smooth Newton methods (see Subsection~\ref{subsec:ss}) for details. The required $\gamma_d$
           will be the experimental data used in \cite{SWalker_BShapiro_RNochetto_2009a} this will give a resulting
           velocity profile which will be used to compare with the pinning $\lambda$ given in \eqref{EWOD} and will be effective 
           in determining the accuracy of the phenomenologically introduced $\lambda$. 

\item Invoke $\lambda$ from \eqref{EWOD} and extend the Euler equations results from 
          \cite{BSchweizer_2005a,JShatah_CZeng_2008a, DCoutand_SShkoller_2007a} to show the 
          existence of solution to \eqref{EWOD}. The equation for $\lambda$ in \eqref{EWOD} is equivalent to
          a complementarity constraint
          \[
              \int_{\bdy\Omega(t)} \del{\bv \cdot \bnu} \del{\mu - \lambda} \le 0 \quad \mbox{for all } \mu \in [-P_0, P_0],
          \]
         such optimal control problems are referred to as mathematical programs with 
         complementarity constraints (MPCC) \cite{MHintermuller_IKopacka_2009a}. There are four different kind of 
         stationarity concepts for MPCC. I will develop a primal-dual semi-smooth Newton strategy and its 
         multigrid version for this problem.
         Studying the control of this problem may have great impact in EWOD 
         techniques.
           
%\item The unwanted effects like hysteresis and pinning of the contact line will be tackled by using the thickness of 
%          the electrode and the shape of the droplets as control variables. 
           
\end{itemize}           
The success of this project will have tremendous impact on adjustable liquid lenses, e-paper, switches for optical
fibers, microfluidics, genomics, forensics. Electrowetting functionality has even been proposed for cleaning oil-spills 
and separating oil-water mixtures. 

%----------%
\subsection{Reverse EWOD} \label{subsec:REWOD}
%----------%
With increasing number of portable devices such as smart-phones, portable media player, there is a desperate
need to provide an easy mean to charge them. There has been various attempts to convert the mechanical energy
to electrical. Recently in \cite{TKrupenkin_JATaylor_2011a} Krupenkin and Taylor 
presented the so-called reverse electrowetting effect which may in fact be useful, for example by 
installing such devices in shoes \cite{TKrupenkin_JATaylor_2011a}. 
I refer to \cite{RODonnell_2008a,PDMitcheson_2008a,GPoulin_2004a,AKhaligh_2010a,SPBeeby_2006a} for the 
existing methods of mechanical-to-electrical energy conversion such as electromagnetic, piezoelectric etc. The
low level output ($\sim10^{-6}-10^{-2} W$) makes them infeasible but REWOD comes to rescue 
\cite{TKrupenkin_JATaylor_2011a}.


In case of the reverse electrowetting the mechanical energy is converted into electrical energy. The droplet and the 
electrode are connected to an external electrical circuit, next an 
external mechanical actuation is used to move the droplet so that the overlap of the droplet and dielectric electrode
is reduced. This results in decrease of the total charge at the droplet liquid-solid interface and the extra charge 
flows back through the circuit which can be used to power the external load.
As pointed out in \cite{TKrupenkin_JATaylor_2011a} there are three main issues which require attention at this point which if taken into account could potentially make the REWOD more effective:
 %
% \begin{itemize}
    %\item 
    First, geometry of the device where fluid actuation is carried out. 
    %\item 
    Second, similar to EWOD, the contact line pinning effect in REWOD could hinder fluid actuation. 
    %\item 
    Finally, surface charge trapping: the thickness of dielectric electrode is $\sim nm$, therefore a few volts can 
              result in electric fields on the scale of $10^6 V cm^{-1}$, this results in immobilization of some
              portion of electrical charge in the dielectric, impeding the collection during the dewetting process
              and therefore reducing production.
% \end{itemize}
 I intend to fix these three issues as follows:
 \begin{itemize}
    \item Study a shape optimization problem with \eqref{EWOD} as constraints. This time my goal will be 
              $V_d$ a desired voltage $\norm{V - V_d}^2_{L^2(0,1)}$ or $E_d$ a desired electric field 
              (for example, see \eqref{eq:fbp_cost}). 
              The free boundary, geometry of the device where the fluid actuation is carried out, and
              the placement of droplets will act as the control variables. 
 
   \item To minimize the contact line pinning effect, I will follow the approach presented in EWOD section.
   
   \item The thickness of the dielectric electrode will be taken as a control variable, this will help to 
             minimize the surface charge trapping. 
               
\end{itemize}
The success of this project has potential to make the reverse electrowetting more effective and increase
the production of power and may even solve the energy crisis for portable devices.

% The models presented in Sections~\ref{sec:stokes_fbp}-\ref{sec:EWOD} are highly nonlinear, 
% it may happen that even after applying adaptive finite element techniques the size of the resulting system
% discrete system is of the order of tens of millions, therefore I propose yet another level of model reduction
% technique to generate reduced order models (ROMs) for optimization problems in next section. 
 
 



